import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

public class ThreadMethod extends Thread{
     //hashmap, которая содержит индексы
    ArrayList<String> filename = IndexBuilder.getFilenames();
    ConcurrentHashMap<String, ArrayList<String>> indexesThreadMap = new ConcurrentHashMap<>();
    int startIndex;
    int endIndex;

    ThreadMethod(ConcurrentHashMap<String, ArrayList<String>> indexesThreadMap, int startIndex, int endIndex) {
        this.indexesThreadMap = indexesThreadMap;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    @Override
    public void run() {
        for (int i = startIndex; i < endIndex; i++) {
            ArrayList<String> result = new ArrayList<>();
            IndexBuilder.GetWordsFromFile(result, filename.get(i));
            for (int j = 0; j < result.size(); j++) {
                ArrayList<String> usedFiles = new ArrayList<>();
                //массив названий файлов, в которых присутствуют наши слова
                if (!indexesThreadMap.containsKey(result.get(j))) {
                    usedFiles.add(filename.get(i));
                    indexesThreadMap.put(result.get(j).toLowerCase(), usedFiles);
                } else if (indexesThreadMap.containsKey(result.get(j))) {
                    indexesThreadMap.get(result.get(j).toLowerCase()).add(filename.get(i));
                }
                Collections.sort(usedFiles);
            }
        }
    }


}
