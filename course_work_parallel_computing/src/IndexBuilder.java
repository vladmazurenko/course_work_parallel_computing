import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


public class IndexBuilder{

    public static void GetWordsFromFile(ArrayList<String> result,String fname) {
            try (FileReader f = new FileReader(fname)) {
                StringBuffer sb = new StringBuffer();
                ArrayList<String> tempresult = new ArrayList<>();
                while (f.ready()) {
                    char c = (char) f.read();
                    if (c == ' ') {
                        tempresult.add(sb.toString());
                        sb = new StringBuffer();
                    } else if (c == ',' || c == '.' || c == '"' || c == '*' || c == ')' || c == '(' || c == ';' || c == '{' || c == '}' || c == ':' || c == '!'|| c == '_' || c == '?' || c == '/' || c == '<' || c == '>') {
                        c = ' ';
                    } else {
                        sb.append(c);
                    }
                }
                if (sb.length() > 0) {
                    tempresult.add(sb.toString());
                }
                for (int i = 0;i<tempresult.size();i++) {
                    result.add(tempresult.get(i).toLowerCase());
               }
                Set<String> set = new HashSet<>(result);
                result.clear();
                result.addAll(set);
                //System.out.println(fname + " : " + result);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    //folder - директория в которой находятся файлы
    //fname - массив названий всех файлов в папке
    public static void listFilesForFolder(final File folder, ArrayList<String> fname) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry, fname);
            } else {
                fname.add(folder + "/" + fileEntry.getName());
            }
        }
    }

    public static ArrayList<String> getFilenames(){
        ArrayList<String> filename = new ArrayList<>();
        for (int g = 1; g < 6; g++) {
            final File folder = new File("/Users/mac/Desktop/data/" + g + "/");
            listFilesForFolder(folder, filename);
            Collections.sort(filename);
        }
        return (filename);
    }

    public static void main(String[] args) throws InterruptedException {
        int THREAD_NUMBER;
        Map<String, ArrayList<String>> indexesMap = new HashMap<>(); //hashmap, которая содержит индексы
        ArrayList<String> filename = getFilenames();
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Enter number of threads: ");
            THREAD_NUMBER = scanner.nextInt();
            if (THREAD_NUMBER > filename.size()) {
                System.out.println("Number of thread has to be lesser than size of the matrix");
            }
        } while (THREAD_NUMBER > filename.size());

        long seqStartTime = System.nanoTime();
        for (int i = 0; i < filename.size(); i++) {
            ArrayList<String> result = new ArrayList<>();
            GetWordsFromFile(result, filename.get(i));
            //System.out.println(result);
            for (int j = 0; j < result.size(); j++) {
                ArrayList<String> usedFiles = new ArrayList<>();
                //массив названий файлов, в которых присутствуют наши слова
                if (!indexesMap.containsKey(result.get(j))) {
                    usedFiles.add(filename.get(i));
                    indexesMap.put(result.get(j), usedFiles);
                } else if (indexesMap.containsKey(result.get(j))) {
                    indexesMap.get(result.get(j)).add(filename.get(i));
                }
                Collections.sort(usedFiles);
            }
        }
        Map<String, ArrayList<String>> sortedMap = new TreeMap<String, ArrayList<String>>(indexesMap);
        long seqEndTime = System.nanoTime();

        HashMap<Integer, Integer[]> threadIndexes = new HashMap<Integer, Integer[]>();
        int SIZE = filename.size();
        for (int i = 0; i < THREAD_NUMBER; i++) {
            if (i == THREAD_NUMBER - 1) {
                threadIndexes.put(i, new Integer[]{SIZE / THREAD_NUMBER * i, SIZE });
            }
            else {
                threadIndexes.put(i, new Integer[]{SIZE  / THREAD_NUMBER * i, SIZE  / THREAD_NUMBER * (i + 1)});
            }
        }

        long threadStartTime = System.nanoTime();
        ConcurrentHashMap<String, ArrayList<String>> indexesThreadMap = new ConcurrentHashMap<>();

        ThreadMethod[] ThreadArray = new ThreadMethod[THREAD_NUMBER];
        for (int i = 0; i < THREAD_NUMBER; i++) {
            ThreadArray[i] = new ThreadMethod(indexesThreadMap,threadIndexes.get(i)[0], threadIndexes.get(i)[1]);
            ThreadArray[i].start();
        }
        for (int i = 0; i < THREAD_NUMBER; i++) {
            ThreadArray[i].join();
        }
        Map<String, ArrayList<String>> sortedThreadMap = new TreeMap<String, ArrayList<String>>(indexesThreadMap);
        long threadEndTime = System.nanoTime();
/*
        Set<Map.Entry<String, ArrayList<String>>> set = sortedMap.entrySet();
        for (Map.Entry<String, ArrayList<String>> me : set) {
            System.out.print(me.getKey() + ": ");
            System.out.println(me.getValue());
        }
*/
        Set<Map.Entry<String, ArrayList<String>>> setThread = sortedThreadMap.entrySet();
        for (Map.Entry<String, ArrayList<String>> me : setThread) {
            System.out.print(me.getKey() + ": ");
            System.out.println(me.getValue());
        }

        System.out.println("Sequence execution time: " + (seqEndTime - seqStartTime) / Math.pow(10, 6) + " milliseconds");
        System.out.println("Thread execution time: " + (threadEndTime - threadStartTime) / Math.pow(10, 6) + " milliseconds");

    }
}
